package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import entities.Car;

public interface RMIInterface extends Remote {

	public double calculateTaxes(Car c) throws RemoteException;

	public double calculateSellingPrice(Car c) throws RemoteException;
}
