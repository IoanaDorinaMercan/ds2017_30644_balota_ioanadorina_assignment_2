package rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import entities.Car;

public class RMIImpl extends UnicastRemoteObject implements RMIInterface {

	public RMIImpl() throws RemoteException {
		super();
	}

	@Override
	public double calculateTaxes(Car c) {
		int sum = 0;
		if (c.getEngineCapacity() <= 1600)
			sum = 8;
		if (c.getEngineCapacity() > 1600 && c.getEngineCapacity() <= 2000)
			sum = 18;
		if (c.getEngineCapacity() > 2000 && c.getEngineCapacity() <= 2600)
			sum = 72;
		if (c.getEngineCapacity() > 2600 && c.getEngineCapacity() <= 3000)
			sum = 144;
		if (c.getEngineCapacity() > 3000)
			sum = 290;
		return (c.getEngineCapacity() / 200.0) * sum;
	}

	@Override
	public double calculateSellingPrice(Car c) {
		return c.getPurchasingPrice() - (c.getPurchasingPrice() / 7) * (2015 - c.getYear());

	}
}
