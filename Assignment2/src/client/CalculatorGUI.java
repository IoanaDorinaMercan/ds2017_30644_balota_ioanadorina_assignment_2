package client;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextArea;

public class CalculatorGUI extends JFrame {

	private JFrame frmClientGui;
	private JTextField fabricationYear;
	private JTextField engineSize;
	private JTextField purchasingPrice;
	private JButton btnCalculateTaxes;
	private JButton btnCalculateSellingPrice;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculatorGUI window = new CalculatorGUI();
					window.frmClientGui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalculatorGUI() {
		setTitle("Client GUI");
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblFabricationYear = new JLabel("Fabrication Year");
		lblFabricationYear.setBounds(30, 43, 120, 14);
		getContentPane().add(lblFabricationYear);
		
		fabricationYear = new JTextField();
		fabricationYear.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
			      if (!((c >= '0') && (c <= '9') ||
			         (c == KeyEvent.VK_BACK_SPACE) ||
			         (c == KeyEvent.VK_DELETE))) {
			        e.consume();
			      }
			}
		});
		fabricationYear.setBounds(206, 40, 86, 20);
		getContentPane().add(fabricationYear);
		fabricationYear.setColumns(10);
		
		JLabel lblEngineSize = new JLabel("Engine size");
		lblEngineSize.setBounds(30, 84, 70, 14);
		getContentPane().add(lblEngineSize);
		
		engineSize = new JTextField();
		engineSize.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
			      if (!((c >= '0') && (c <= '9') ||
			         (c == KeyEvent.VK_BACK_SPACE) ||
			         (c == KeyEvent.VK_DELETE))) {
			        e.consume();
			      }
			}
		});
		engineSize.setBounds(206, 81, 86, 20);
		getContentPane().add(engineSize);
		engineSize.setColumns(10);
		
		JLabel lblPurchasingPrice = new JLabel("Purchasing Price");
		lblPurchasingPrice.setBounds(30, 132, 174, 14);
		getContentPane().add(lblPurchasingPrice);
		
		purchasingPrice = new JTextField();
		purchasingPrice.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
			      if (!((c >= '0') && (c <= '9') ||
			         (c == KeyEvent.VK_BACK_SPACE) ||
			         (c == KeyEvent.VK_DELETE)||(c=='.'))
			    		  ) {
			   
			        e.consume();
			      }
			}
		});
		purchasingPrice.setBounds(206, 129, 86, 20);
		getContentPane().add(purchasingPrice);
		purchasingPrice.setColumns(10);
		
		btnCalculateTaxes = new JButton("Calculate Taxes");
		btnCalculateTaxes.setBounds(30, 167, 158, 23);
		getContentPane().add(btnCalculateTaxes);
		
		btnCalculateSellingPrice = new JButton("Calculate Selling Price");
		btnCalculateSellingPrice.setBounds(206, 167, 167, 23);
		getContentPane().add(btnCalculateSellingPrice);
		
		textArea = new JTextArea();
		textArea.setBounds(30, 195, 343, 56);
		getContentPane().add(textArea);
	}

	
	
	
	public String getFabricationYear()
	{
		return fabricationYear.getText();
	}
	
	public String getEngineSize()
	{
		return engineSize.getText();
	}
	
	public String gePurchasingPrice()
	{
		return purchasingPrice.getText();
	}
	
	public void addBtnCalculateTaxesActionListener(ActionListener e) {
		btnCalculateTaxes.addActionListener(e);
	}
	
	public void addBtnCalculateSellingPriceActionListener(ActionListener e) {
		btnCalculateSellingPrice.addActionListener(e);
	}
	
	public void printMessage(String message)
	{
		textArea.setText(message);
	}
	
	public void clear()
	{
		fabricationYear.setText("");
		engineSize.setText("");
		purchasingPrice.setText("");
		textArea.setText("");
	}
}
