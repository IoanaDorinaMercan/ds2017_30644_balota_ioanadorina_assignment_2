package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

import entities.Car;
import rmi.RMIInterface;

public class CalculatorController {

	private CalculatorGUI calculatorGUI;
	RMIInterface rmiInterface;

	public CalculatorController() {
		calculatorGUI = new CalculatorGUI();
		calculatorGUI.setVisible(true);
		calculatorGUI.addBtnCalculateTaxesActionListener(new Taxes());
		calculatorGUI.addBtnCalculateSellingPriceActionListener(new SellingPrice());
	}

	public void displayErrorMessage(String message) {
		calculatorGUI.clear();
		JOptionPane.showMessageDialog(calculatorGUI, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void printInformations(String message) {
		calculatorGUI.printMessage(message);
	}

	class Taxes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			// year and engine size necessary for calculation
			int year = 0, size = 0;

			// get fabrication year from text field
			try {
				year = Integer.valueOf(calculatorGUI.getFabricationYear());
				if (year < 0)
					displayErrorMessage("Fabrication year must be an pozitive integer!");
			} catch (Exception ex) {
				displayErrorMessage("Fabrication year must be an pozitive integer!");
			}

			// get engine size from text field
			try {
				size = Integer.valueOf(calculatorGUI.getEngineSize());
				if (size < 0) {
					displayErrorMessage("Engine size year must be an pozitive integer!");
				}
			} catch (Exception ex) {
				displayErrorMessage("Engine size year must be an pozitive integer!");
			}

			Car c = new Car(year, size, 0);

			// connecting to rmi
			try {
				rmiInterface = (RMIInterface) Naming.lookup("//localhost/MyServer");
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (RemoteException e1) {
				e1.printStackTrace();
			} catch (NotBoundException e1) {
				e1.printStackTrace();
			}

			// calculate taxes
			try {
				printInformations("Taxes " + rmiInterface.calculateTaxes(c));
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}

	class SellingPrice implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			double purchasingPrice = 0;
			int year = 0;

			// get fabrication year from text field
			try {
				year = Integer.valueOf(calculatorGUI.getFabricationYear());
				if (year < 0)
					displayErrorMessage("Fabrication year must be an pozitive integer!");
			} catch (Exception ex) {
				displayErrorMessage("Fabrication year must be an pozitive integer!");
			}

			// get purchasing price from text field
			try {
				purchasingPrice = Double.valueOf(calculatorGUI.gePurchasingPrice());
				if (purchasingPrice < 0) {
					displayErrorMessage("Purchasing price must be an pozitive double!");
				}
			} catch (Exception ex) {
				displayErrorMessage("Purchasing price must be an pozitive double!");
			}
			Car c = new Car(year, 0, purchasingPrice);

			// connecting to rmi
			try {
				rmiInterface = (RMIInterface) Naming.lookup("//localhost/MyServer");
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (RemoteException e1) {
				e1.printStackTrace();
			} catch (NotBoundException e1) {
				e1.printStackTrace();
			}

			// calculate selling price using rmi
			try {
				printInformations("SellingPrice " + rmiInterface.calculateSellingPrice(c));
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
}
