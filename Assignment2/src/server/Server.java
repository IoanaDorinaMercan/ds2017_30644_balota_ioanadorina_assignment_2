package server;

import java.rmi.Naming;
import rmi.RMIImpl;

public class Server {

	public static void main(String[] args) {

		try {

			Naming.rebind("//localhost/MyServer", new RMIImpl());
			System.err.println("Server ready");

		} catch (Exception e) {

			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();

		}

	}
}
